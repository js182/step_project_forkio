import gulp from "gulp";
import sass from "gulp-sass";
import dartSass from "sass";
import browserSync from "browser-sync";
import clean from "gulp-clean";
import cleanCSS from "gulp-clean-css";
import autoprefixer from "gulp-autoprefixer";
import concat from "gulp-concat";
import uglify from "gulp-uglify";
import imagemin from "gulp-imagemin";
import fileInclude from "gulp-file-include";

const sassCompiler = sass(dartSass);

export function styles() {
  return gulp
    .src("src/scss/**/*.scss")
    .pipe(sassCompiler())
    .pipe(
      autoprefixer({
        overrideBrowserslist: ["last 2 versions"],
        cascade: false,
      })
    )
    .pipe(concat("style.min.css"))
    .pipe(cleanCSS())
    .pipe(gulp.dest("dist"))
    .pipe(browserSync.stream());
}

export function scripts() {
  return gulp
    .src("src/js/**/*.js")
    .pipe(concat("scripts.min.js"))
    .pipe(uglify())
    .pipe(gulp.dest("dist"))
    .pipe(browserSync.stream());
}

export function images() {
  return gulp
    .src("src/img/**/*.*")
    .pipe(imagemin())
    .pipe(gulp.dest("dist/img"))
    .pipe(browserSync.stream());
}

export function svg() {
  return gulp
    .src("src/svg/**/*")
    .pipe(gulp.dest("dist/svg"))
    .pipe(browserSync.stream());
}

export function cleanDist() {
  return gulp.src("dist", { allowEmpty: true, read: false }).pipe(clean());
}

export async function watch() {
  browserSync.init({
    server: {
      baseDir: "./dist",
    },
  });
  gulp.watch("src/scss/**/*.scss", styles);
  gulp.watch("src/img/**/*.*", images);
  gulp.watch("src/svg/**/*.*", svg);
  gulp.watch("src/js/**/*.js", scripts);
  gulp.watch("src/**/*.html", html).on("change", browserSync.reload);
}

export function html() {
  return gulp
    .src("./*.html")
    .pipe(
      fileInclude({
        prefix: "@@",
        basepath: "src/html",
      })
    )
    .pipe(gulp.dest("dist"))
    .pipe(browserSync.stream());
}

export const dev = gulp.series(
  cleanDist,
  gulp.parallel(styles, scripts, images, html, svg),
  watch
);

export const build = gulp.series(
  cleanDist,
  gulp.parallel(styles, scripts, images, html, svg)
);

export default gulp.series(dev);
