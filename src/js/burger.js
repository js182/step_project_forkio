document.addEventListener('DOMContentLoaded', function () {
    const burger = document.querySelector('.burger');
    const nav = document.querySelector('.nav');

    burger.addEventListener('click', function (event) {
        event.stopPropagation();
        burger.classList.toggle('burger--active');
        burger.classList.toggle('burger--crossed');
        nav.classList.toggle('nav--open');
    });

    const navLinks = document.querySelectorAll('.nav a');
    for (const link of navLinks) {
        link.addEventListener('click', function () {
            if (nav.classList.contains('nav--open')) {
                burger.classList.remove('burger--active');
                burger.classList.remove('burger--crossed');
                nav.classList.remove('nav--open');
            }
        });
    }

    document.addEventListener('click', function (event) {
        const isClickInsideBurger = burger.contains(event.target);
        if (!isClickInsideBurger && nav.classList.contains('nav--open')) {
            burger.classList.remove('burger--active');
            burger.classList.remove('burger--crossed');
            nav.classList.remove('nav--open');
        }
    });
});
